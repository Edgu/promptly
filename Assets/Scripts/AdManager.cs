﻿using GoogleMobileAds.Api;
using UnityEngine;

public class AdManager : MonoBehaviour {

	private static AdManager INSTANCE;

	private string adUnitId;
	private BannerView bannerView;

	void Start() {
		INSTANCE = this;
		DontDestroyOnLoad(INSTANCE);
		adUnitId = "unexpected_platform";

#if UNITY_ANDROID
			adUnitId = "ca-app-pub-8718660884893756/2072108464";
#elif UNITY_IPHONE
			adUnitId = "ca-app-pub-8718660884893756/5115050683";
#endif
		RequestBanner();
	}

	public void RequestBanner() {
		//adUnitId = "ca-app-pub-3940256099942544/6300978111"; // Test ad unit
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		AdRequest adRequest = new AdRequest.Builder().Build();
		bannerView.LoadAd(adRequest);
	}

	public void ShowAd() {
		bannerView.Show();
	}

	public void HideAd() {
		bannerView.Hide();
	}

	public static AdManager GetInstance() {
		return INSTANCE;
	}

}
