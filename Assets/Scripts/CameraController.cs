﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private Camera controlledCamera;

    void Start() {
        controlledCamera = GetComponent<Camera>();
    }
	
	void Update() {
        controlledCamera.orthographicSize = Mathf.Clamp(controlledCamera.orthographicSize - Input.GetAxis("Mouse ScrollWheel"), 0.1f, 1);
    }

}