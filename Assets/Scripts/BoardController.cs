﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class BoardController : MonoBehaviour {

	// Editor fields

	[SerializeField]
	private GameObject[] hitBox;

	[SerializeField]
	private GameObject[] laneButtons;

	[SerializeField]
	private GameObject[] shapeButtons;

	[SerializeField]
	private GameObject[] spawns;

	[SerializeField]
	private float speed;

	[SerializeField]
	private AudioClip hitSound;

	[SerializeField]
	private AudioClip bgSound;

	[SerializeField]
	private Text startText;

	[SerializeField]
	private Text scoreText;

	[SerializeField]
	private Text rateText;

	[SerializeField]
	private Text[] recordText;

	///////////////////////

	// Constants
	private static readonly string[] KEYS = new string[] {"HSCK", "HRAK", "HSPK"};

	private const int SCORE_DECREMENT_STEP = 50;
	private const int HITS_TO_INCREASE_LEVEL = 2;
	private const int TICKS_PER_SECOND = 60;
	private const int STARTING_RATE = 0;
	private static readonly Color[] COLORS = new Color[] {new Color(1, (225f / 255f), 0f, 1f), new Color(0f, (225f / 255f), 0f, 1f), new Color((225f / 255f), 0f, 0f, 1f), new Color(0f, 0f, 0f, 0f)};
	private const int MAX_RATE = 40;
	private const int POINTS_FOR_HIT = 1000;
	private const int HIT_COUNT_OFFSET = 10;

	private SpriteRenderer[] laneSpriteRenderer;
	private Button selectedShapeButton;
	private int selectedLaneButton;
	private Vector2 velocity;
	private List<GameObject> shapes;
	private int updateCount;
	private int scoreDecrement;
	private int hitCount;
	private AudioSource audioSource;
	private bool playing;
	private int restart;

	private int[] gameSettings;

	private AdManager adManager;

	void Start() {
		Application.targetFrameRate = 60;
		adManager = AdManager.GetInstance();
		// Create, get record score, rate.
		for (int i = 0; i < recordText.Length; i++) {
			string key = KEYS[i];
			if (PlayerPrefs.HasKey(key)) {
				recordText[i].text = PlayerPrefs.GetInt(key).ToString();
			} else {
				PlayerPrefs.SetInt(key, 0);
				PlayerPrefs.Save();
			}
		}

		gameSettings = new int[2];

		restart = 1;
		playing = false;

		audioSource = GetComponent<AudioSource>();
		audioSource.loop = true;
		audioSource.clip = bgSound;
		audioSource.Play();

		rateText.text = (gameSettings[1] - STARTING_RATE).ToString();

		selectedLaneButton = -1;
		Application.runInBackground = true;
		scoreText.text = gameSettings[0].ToString();
		laneSpriteRenderer = new SpriteRenderer[hitBox.Length];

		for (int i = 0; i < hitBox.Length; i++) {
			SpriteRenderer sr = hitBox[i].GetComponent<SpriteRenderer>();
			sr.color = COLORS[COLORS.Length - 1];
			laneSpriteRenderer[i] = sr;
		}

		// Setup events
		for (int i = 0; i < shapeButtons.Length; i++) {
			Button button = shapeButtons[i].GetComponent<Button>();

			EventTrigger trigger = button.GetComponent<EventTrigger>();

			EventTrigger.Entry enterEntry = new EventTrigger.Entry() {
				eventID = EventTriggerType.PointerEnter
			};
			enterEntry.callback.AddListener(delegate {
				OnPressShapeButton(button);
			});
			trigger.triggers.Add(enterEntry);

			EventTrigger.Entry exitEntry = new EventTrigger.Entry() {
				eventID = EventTriggerType.PointerExit
			};
			exitEntry.callback.AddListener(delegate {
				OnReleaseShapeButton(button);
			});
			trigger.triggers.Add(exitEntry);
		}

		for (int i = 0; i < laneButtons.Length; i++) {
			int argument = i;
			Button button = laneButtons[i].GetComponent<Button>();

			EventTrigger trigger = button.GetComponent<EventTrigger>();
			EventTrigger.Entry downEntry = new EventTrigger.Entry() {
				eventID = EventTriggerType.PointerEnter
			};
			downEntry.callback.AddListener(delegate {
				OnPressLaneButton(argument);
			});
			trigger.triggers.Add(downEntry);

			EventTrigger.Entry upEntry = new EventTrigger.Entry() {
				eventID = EventTriggerType.PointerExit
			};
			upEntry.callback.AddListener(delegate {
				OnReleaseLaneButton(argument);
			});
			trigger.triggers.Add(upEntry);
		}

		velocity = new Vector2(0f, 0f);
		shapes = new List<GameObject>();
	}

	private void Update() {
		if (!playing) {
			if (selectedLaneButton == -1 && selectedShapeButton == null) {
				// Start new game
				if (Input.GetMouseButtonDown(0)) {
					adManager.HideAd();

					gameSettings[0] = 0;
					gameSettings[1] = STARTING_RATE;
					scoreText.text = gameSettings[0].ToString();
					rateText.text = (gameSettings[1] - STARTING_RATE).ToString();

					foreach (GameObject go in shapes) {
						Destroy(go);
					}

					shapes.Clear();

					velocity.y = -Mathf.Abs(speed);

					startText.enabled = false;

					SetRecordTextStatus(false);
					playing = true;
				}
			}
		}
	}

	private void FixedUpdate() {
		if (playing) {
			scoreText.text = (gameSettings[0] -= scoreDecrement).ToString();

			if (updateCount < (TICKS_PER_SECOND - gameSettings[1])) {
				updateCount++;
			} else {
				GameObject o = spawns[Random.Range(0, spawns.Length)];
				GameObject go = Instantiate(o, o.transform.parent);
				go.transform.GetChild(0).GetComponent<SpriteRenderer>().color = COLORS[Random.Range(0, COLORS.Length - 1)];
				shapes.Add(go);
				updateCount = 0;
			}

			if (gameSettings[1] < MAX_RATE) {
				if (hitCount == HITS_TO_INCREASE_LEVEL) {
					gameSettings[1] += 2;
					rateText.text = (gameSettings[1] - STARTING_RATE).ToString();
					hitCount = 0;
				}
			}

			for (int i = 0; i < shapes.Count; i++) {
				GameObject go = shapes[i];
				Rigidbody2D rb = go.GetComponent<Rigidbody2D>();

				bool hit = false;

				//check if gucci
				GameObject lane = go.transform.parent.gameObject;
				GameObject slot = lane.transform.GetChild(0).gameObject;
				SpriteRenderer slotShape = slot.transform.GetChild(0).GetComponent<SpriteRenderer>();

				float diff = Mathf.Abs(go.transform.position.y - slot.transform.position.y);

				if (diff <= 0.2f) {
					if (slotShape.color.Equals(go.transform.GetChild(0).GetComponent<SpriteRenderer>().color)) {
						hit = true;
						gameSettings[0] += POINTS_FOR_HIT;
						hitCount++;
						audioSource.PlayOneShot(hitSound);
					}
				}

				if (hit) {
					shapes.RemoveAt(i);
					Destroy(go);
					i--;
					continue;
				} else if (rb.transform.localPosition.y <= -4.2f) {
					playing = false;
					startText.enabled = true;
					scoreDecrement = 0;
					restart++;
					hitCount = 0;

					for (int j = 0; j < recordText.Length; j++) {
						string key = KEYS[j];
						if (PlayerPrefs.HasKey(key)) {
							int savedValue = PlayerPrefs.GetInt(key);
							int currentValue = gameSettings[j];

							if (currentValue > savedValue) {
								PlayerPrefs.SetInt(key, currentValue);
								recordText[j].text = currentValue.ToString();
							}
						}
						PlayerPrefs.Save();
					}

					SetRecordTextStatus(true);

					if (restart % 5 == 0 && Advertisement.IsReady()) {
						adManager.RequestBanner();
						Advertisement.Show();
						return;
					} else {
						adManager.ShowAd();
					}
				}

				//move
				rb.MovePosition(rb.position + velocity * Time.smoothDeltaTime);
			}
		}
	}

	private void OnReleaseLaneButton(int index) {
		Button button = laneButtons[index].GetComponent<Button>();
		button.enabled = false;
		button.enabled = true;

		if (index == selectedLaneButton) {
			laneSpriteRenderer[selectedLaneButton].color = COLORS[COLORS.Length - 1];
			selectedLaneButton = -1;
			scoreDecrement = 0;
		}
	}

	private void OnPressLaneButton(int index) {
		laneButtons[index].GetComponent<Button>().Select();
		if (selectedLaneButton == -1) {
			selectedLaneButton = index;
			if (selectedShapeButton != null) {
				laneSpriteRenderer[index].color = selectedShapeButton.transform.GetChild(0).GetComponent<Image>().color;
				if (scoreDecrement != SCORE_DECREMENT_STEP) {
					scoreDecrement = SCORE_DECREMENT_STEP;
				}
			}
		}
	}

	private void OnPressShapeButton(Button button) {
		selectedShapeButton = button;
		button.Select();
		if (selectedLaneButton != -1) {
			int temp = selectedLaneButton;
			selectedLaneButton = -1;
			OnPressLaneButton(temp);
		}
	}

	private void OnReleaseShapeButton(Button button) {
		if (Application.isMobilePlatform) {
			button.enabled = false;
			button.enabled = true;

			selectedShapeButton = null;
			if (selectedLaneButton != -1) {
				laneSpriteRenderer[selectedLaneButton].color = COLORS[COLORS.Length - 1];
				scoreDecrement = 0;
			}
		}
	}

	private void SetRecordTextStatus(bool status) {
		for (int i = 0; i < startText.transform.childCount; i++) {
			Transform child = startText.transform.GetChild(i);
			child.GetComponent<Text>().enabled = status;

			// Get NumberText of the child.
			child.GetChild(0).GetComponent<Text>().enabled = status;
		}
	}

}